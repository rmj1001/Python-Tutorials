# Welcome to my Python Tutorial Repository

## Table of Contents
1) [About this repository](#About-this-repository "About this Repository")
2) [Disclaimer](#Disclaimer "Disclaimer")
3) [How to read the files](#How-to-read-the-files "How to read the files")
4) [How to install Python 3](#How-to-install-Python "How to install Python")
5) [How to install VS Code](#How-to-install-VS-Code "How to install VS Code")
6) [File Table of Contents](#File-Table-of-Contents "File Table of Contents")

## About this repository
I was watching a video from FreeCodeCamp on Youtube. It was a
4 1/2 hour course on Python. I decided to create a repository of some of their code examples, and publish it to Github. The
numbers before the file name show the order the code examples
came in the video.

Click [Here](https://www.youtube.com/watch?v=rfscVS0vtbw "Tutorial Video") to watch the tutorial video. (Full video is 4.5 hours.)

## Disclaimer
This is not an official repository from FreeCodeCamp,
nor do they endorse this repo. These are also example python
files, and do not serve a distince purpose.

## How to read the files
All Python Tutorial files have comments in the code, following a #. The comments won't run in the Python Shell. To run the files, you must have Python 3 installed. It is recommended to add Python to your PATH.

Also, I recommend using VS Code to read the files. Sublime Text, Atom, and other code editors or IDEs will work, but VS code is much more featured, in my opinion.

Python is included in Linux and Mac, but as Python 2. You need to install Python 3.

## How to install Python 3
Click [Here](https://realpython.com/installing-python/ "Python Installation Tutorial") to find a tutorial to install Python 3 on your computer.

## How to install VS Code
Click [Here](https://code.visualstudio.com/docs/setup/setup-overview "VS Code Installation Tutorial") to find out how to install VS Code on your computer.

## File Table of Contents
1)  [Hello World](/Tutorials-1-9/1-HelloWorld.py "Hello World")
2)  [Variables](/Tutorials-1-9/2-Variables.py "Variables")
3)  [Strings](/Tutorials-1-9/3-Strings.py "Strings")
4)  [Shape](/Tutorials-1-9/4-Shape.py "Shape")
5)  [List](/Tutorials-1-9/5-List.py "List")
6)  [List Functions](/Tutorials-1-9/6-List-Functions.py "List Functions")
7)  [Tuples](/Tutorials-1-9/7-Tuples.py "Tuples")
8)  [Functions](/Tutorials-1-9/8-Functions.py "Functions")
9)  [Return Statements](/Tutorials-1-9/9-Return-Statement.py "Return Statements")
10) [If Statements](/Tutorials-10-19/10-If-Statements.py "If Statements")
11) [If Statement Comparisons](/Tutorials-10-19/11-If-Statement-Comparisons.py "If Statement Comparisons")
12) [Calculator](/Tutorials-10-19/12-Calculator.py "Basic Calculator")
13) [Dictionaries](/Tutorials-10-19/13-Dictionaries.py "Dictionaries")
14) [While Loops](/Tutorials-10-19/14-While-Loop.py "While Loops")
15) [Guessing Game](/Tutorials-10-19/15-Guessing-Game.py "Guessing Game")
16) [For Loops](/Tutorials-10-19/16-For-Loop.py "For Loop")
17) [Exponent Function](/Tutorial-10-19/17-Exponent-Function.py "Exponent Function")
18) [2D Lists and Nested Loops](/Tutorials-10-19/18-2D-Lists-Nested-Loops.py "2D Lists and Nested Loops")
19) [Translator](/Tutorials-10-19/19-Translator.py "Translator")
20) [Comments](/Tutorials-20-29/20-Comments.py "Comments")
21) [Try/Except](/Tutorials-20-29/21-Try-Except.py "Try/Except")
22) [Readings](/Tutorials-20-29/22-Reading-Files "Reading Files")
23) [Writings](/Tutorials-20-29/23-Writing-Files "Writing Files")
24) [Modules](/Tutorials-20-29/24-Modules "Modules")
25) [Classes and Objects](/Tutorials-20-29/25-Classes-Objects "Classes and Objects")
26) [Multiple Choice Quiz](/Tutorials-20-29/26-Quiz "Multiple Choice Quiz")
27) [Object Functios](/Tutorials-20-29/27-Object-Functions "Object Functions")
28) [Inheritance](/Tutorials-20-29/28-Inheritance "Inheritance")

#### Created by: Roy Conn
#### Code examples from FreeCodeCamp